/**
 * @file simulations.c
 * @brief Scénarios de simulation
 *
 * Ce fichier contient les scénarios de simulation du TP 2 de simulation.
 * Les scénarios de simulation sont les suivants :
 * - Simulation d'une loi uniforme
 * - Simulation d'une loi discrète empirique
 * - Simulation d'une loi discrète empirique générique
 * - Simulation d'une loi exponentielle négative
 * - Simulation de la somme de 40 dés
 * - Simulation avec la méthode de Box-Muller
 * Les scénarios de simulation sont définis dans des fonctions séparées.
 * 
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"
#include "functions.h"

#define M_PI 3.14159265358979323846

/**
 * @brief Simulation d'une loi uniforme
 *
 * Cette fonction simule une loi uniforme sur l'intervalle [a, b].
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_uniform(int n_simu)
{
    double a = -98.0;
    double b = 57.7;

    for (int i = 0; i < n_simu; i++)
    {
        double res = uniform(a, b);
        printf("%.1f\n", res);
    }
}

/**
 * @brief Simulation d'une loi discrète empirique
 *
 * Cette fonction simule une loi discrète empirique avec 3 classes.
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_class(int n_simu)
{
    int nb_A = 0;
    int nb_B = 0;
    int nb_C = 0;
    for (int i = 0; i < n_simu; i++)
    {
        char res = discrete_empirical();
        printf("%c\n", res);
        if (res == 'A')
            nb_A++;
        else if (res == 'B')
            nb_B++;
        else
            nb_C++;
    }
    double p_A = (double)nb_A / n_simu;
    double p_B = (double)nb_B / n_simu;
    double p_C = (double)nb_C / n_simu;
    fprintf(stderr, "p_A = %.2f, p_B = %.2f, p_C = %.2f\n", p_A, p_B, p_C);
}

/**
 * @brief Simulation d'une loi discrète empirique générique
 *
 * Cette fonction simule une loi discrète empirique avec un nombre quelconque de classes.
 * Calcul des probabilités cumulées des classes.
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_discrete_emp_generic(int n_simu)
{
    int classes[6] = {100, 400, 600, 400, 100, 200}; // En utilisant les données HDL du cours.
    double *cum_probs = discrete_empirical_generic(6, classes);
    for (int i = 0; i < 6; i++)
    {
        printf("%.2f\n", cum_probs[i]);
    }
    int *nb_classes = (int *)malloc(6 * sizeof(int));
    for (int i = 0; i < 6; i++)
    {
        nb_classes[i] = 0;
    }
    for (int i = 0; i < n_simu; i++)
    {
        double u = genrand_real2();
        for (int j = 0; j < 6; j++)
        {
            if (u < cum_probs[j])
            {
                nb_classes[j]++;
                break;
            }
        }
    }
    for (int i = 0; i < 6; i++)
    {
        printf("%d\n", nb_classes[i]);
    }
    free(cum_probs);
    free(nb_classes);
}

/**
 * @brief Simulation d'une loi exponentielle négative
 *
 * Cette fonction simule une loi exponentielle négative avec un paramètre lambda.
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_neg_exp(int n_simu)
{
    int bins[21] = {0}; // Tableau pour compter le nombre de valeurs dans chaque intervalle
    double lambda = 10.0;

    for (int i = 0; i < n_simu; i++)
    {
        double value = negExp(lambda);
        int bin_index = (int)value;
        if (bin_index < 20)
        {
            bins[bin_index]++;
        }
        else
        {
            bins[20]++;
        }
    }

    for (int i = 0; i < 20; i++)
    {
        printf("Bin %d (%.1f to %.1f): %d\n", i, (double)i, (double)(i + 1), bins[i]);
    }
    printf("Bin 20 (20 and above): %d\n", bins[20]);
}

/**
 * @brief Simulation de la somme de 40 dés
 *
 * Cette fonction simule la somme de 40 dés.
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_sum_dice(int n_simu)
{
    int bins[241] = {0}; // Tableau pour compter le nombre de valeurs dans chaque intervalle
    double sum_total = 0.0;
    double sum_squared = 0.0;

    for (int i = 0; i < n_simu; i++)
    {
        int sum = 0;
        for (int j = 0; j < 40; j++)
        {
            sum += genrand_int32() % 6 + 1;
        }
        bins[sum - 40]++;
        sum_total += sum;
        sum_squared += sum * sum;
    }

    double mean = sum_total / n_simu;
    double variance = (sum_squared / n_simu) - (mean * mean);
    double stddev = sqrt(variance);

    printf("Moyenne: %.2f\n", mean);
    printf("Écart-type: %.2f\n", stddev);

    for (int i = 0; i < 241; i++)
    {
        printf("%d: %d\n", i + 40, bins[i]);
    }
}

/**
 * @brief Simulation de la méthode de Box-Muller
 *
 * Cette fonction simule la méthode de Box-Muller pour générer des variables aléatoires normales.
 * Elle affiche la moyenne, l'écart-type et le nombre de valeurs dans chaque intervalle de largeur 1.
 * Les intervalles sont [-5, -4], [-4, -3], ..., [4, 5].
 *
 * @param n_simu Le nombre de simulations à effectuer
 * @return void
 */
void simu_box_muller(int n_simu)
{
    double sum = 0.0;
    double sum_squared = 0.0;
    int bins[20] = {0};             // Tableau pour compter le nombre de valeurs dans chaque intervalle
    double bin_width = 10.0 / 20.0; // Largeur de chaque intervalle

    for (int i = 0; i < n_simu; i++)
    {
        double u1 = genrand_real2();
        double u2 = genrand_real2();
        double z1 = sqrt(-2 * log(u1)) * cos(2 * M_PI * u2);
        double z2 = sqrt(-2 * log(u1)) * sin(2 * M_PI * u2);

        // Calcul de la moyenne et de la variance
        sum += z1;
        sum_squared += z1 * z1;
        sum += z2;
        sum_squared += z2 * z2;

        // Mise à jour des intervalles pour z1
        if (z1 >= -5 && z1 < 5)
        {
            int bin_index = (int)((z1 + 5) / bin_width);
            bins[bin_index]++;
        }

        // Mise à jour des intervalles pour z2
        if (z2 >= -5 && z2 < 5)
        {
            int bin_index = (int)((z2 + 5) / bin_width);
            bins[bin_index]++;
        }
    }

    double mean = sum / (2 * n_simu);
    double variance = (sum_squared / (2 * n_simu)) - (mean * mean);
    double stddev = sqrt(variance);

    printf("Moyenne: %.2f\n", mean);
    printf("Écart-type: %.2f\n", stddev);

    for (int i = 0; i < 20; i++)
    {
        printf("Intervalle %d (%.2f à %.2f): %d\n", i, -5 + i * bin_width, -5 + (i + 1) * bin_width, bins[i]);
    }
}
