/**
 * @file functions.c
 * @brief Fonctions de simulation
 *
 * Ce fichier contient les fonctions de simulation.
 *
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"

/**
 * @brief Simulation d'une loi uniforme
 *
 * Cette fonction simule une loi uniforme sur l'intervalle [a, b].
 *
 * @param a La borne inférieure de l'intervalle
 * @param b La borne supérieure de l'intervalle
 * @return double La valeur simulée
 */
double uniform(double a, double b)
{
    return a + (b - a) * genrand_real2();
}

/**
 * @brief Simulation d'une loi discrète empirique
 *
 * Cette fonction simule une loi discrète empirique avec 3 classes.
 * Les classes sont A, B et C avec les probabilités suivantes :
 * - A : 0.35
 * - B : 0.45
 * - C : 0.20
 *
 * @return void
 */
char discrete_empirical()
{
    double u = genrand_real2();
    if (u < 0.35)
        return 'A';
    else if (u < 0.80)
        return 'B';
    else
        return 'C';
}

/**
 * @brief Simulation d'une loi discrète empirique générique
 *
 * Cette fonction simule une loi discrète empirique avec un nombre quelconque de classes.
 *
 * @param size Le nombre de classes
 * @param classes Les effectifs des classes
 * @return double* Les probabilités cumulées des classes
 */
double *discrete_empirical_generic(int size, int *classes)
{
    // Création d'un tableau pour chaque classe
    double *probs = (double *)malloc(size * sizeof(double));
    double sum = 0.0;
    for (int i = 0; i < size; i++)
    {
        sum += classes[i];
    }
    for (int i = 0; i < size; i++)
    {
        probs[i] = (double)classes[i] / sum;
    }

    // Création d'un tableau de probabilité cumulée
    double *cum_probs = (double *)malloc(size * sizeof(double));
    cum_probs[0] = probs[0];
    for (int i = 1; i < size; i++)
    {
        cum_probs[i] = cum_probs[i - 1] + probs[i];
    }

    free(probs);
    return cum_probs;
}

/**
 * @brief Simulation d'une loi exponentielle
 *
 * Cette fonction simule une loi exponentielle.
 *
 * @param inMean La moyenne de la loi exponentielle
 * @return double La valeur simulée
 */
double negExp(double inMean)
{
    return -inMean * log(1 - genrand_real2());
}
