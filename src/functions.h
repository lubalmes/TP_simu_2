#ifndef FUNCTIONS_H
#define FUNCTIONS_H

double uniform(double a, double b);
char discrete_empirical();
double *discrete_empirical_generic(int size, int *classes);
double negExp(double inMean);

#endif // FUNCTIONS_H