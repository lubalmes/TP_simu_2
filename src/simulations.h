#ifndef SIMULATIONS_H
#define SIMULATIONS_H

void simu_uniform(int n_simu);
void simu_class(int n_simu);
void simu_discrete_emp_generic(int n_simu);
void simu_neg_exp(int n_simu);
void simu_sum_dice(int n_simu);
void simu_box_muller(int n_simu);

#endif // SIMULATIONS_H