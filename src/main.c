/**
 * @file main.c
 * @brief Fichier principal du programme du TP 2 de simulation
 *
 * Ce fichier contient la fonction principale du programme du TP de simulation 2.
 * Ainsi que la fonction d'initialisation du générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"
#include "simulations.h"

/**
 * @brief Fonction d'initialisation du générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * Cette fonction initialise le générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * @return void
 */
void init_mt19937ar()
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);
}

/**
 * @brief Fonction principale du programme
 *
 * Cette fonction est la fonction principale du programme.
 *
 * @param argc Le nombre d'arguments passés au programme
 * @param argv Les arguments passés au programme
 * @return int Le code de retour du programme
 */
int main()
{
    init_mt19937ar();

    // simu_uniform(1000000);
    // simu_sum_dice(1000000);
    // simu_neg_exp(1000000);
    // simu_box_muller(1000000);

    return 0;
}
