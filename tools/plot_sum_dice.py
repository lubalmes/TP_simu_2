import matplotlib.pyplot as plt

def read_data(file_path):
    data = []
    with open(file_path, 'r') as file:
        for line in file:
            x, y = map(int, line.strip().split(':'))
            data.append((x, y))
    return data

def plot_data(data):
    x_values = [x for x, y in data]
    y_values = [y for x, y in data]

    plt.bar(x_values, y_values)
    plt.xlabel('Sum results')
    plt.ylabel('Nb of occurrences')
    plt.title('Results of the sum of 40 dice rolls (1000000 simulations)')
    plt.show()

if __name__ == "__main__":
    file_path = 'results/simu_sum_dice_1000000.out'
    data = read_data(file_path)
    plot_data(data)