import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

# Fonction pour lire les données du fichier
def read_data(file_path):
    bins = []
    counts = []
    with open(file_path, 'r') as f:
        for line in f:
            parts = line.split(':')
            bin_info = parts[0].split('(')[-1].split(')')[0].split('to')
            lower_bound = float(bin_info[0].strip())
            upper_bound = float(bin_info[1].strip())
            count = int(parts[1].strip())
            bins.append((lower_bound + upper_bound) / 2)  # Milieu de l'intervalle
            counts.append(count)
    return bins, counts

# Fonction pour tracer les barres et la gaussienne
def plot_histogram_with_gaussian(bins, counts, mean, std_dev):
    # Tracé de l'histogramme
    plt.bar(bins, counts, width=bins[1] - bins[0], color='blue', alpha=0.6, label='Data')

    # Calcul de la gaussienne avec les paramètres fournis
    x = np.linspace(min(bins), max(bins), 1000)
    gaussian = norm.pdf(x, mean, std_dev)
    
    # Ajustement pour que la gaussienne corresponde à l'échelle des barres
    scale_factor = max(counts) / max(gaussian)
    plt.plot(x, gaussian * scale_factor, 'r-', label=f'Gaussian (mean={mean}, std={std_dev})')

    plt.xlabel('Bins')
    plt.ylabel('Counts')
    plt.title('Histogram with Gaussian overlay')
    plt.legend()
    plt.grid(True)
    plt.show()

# Exemple d'utilisation
file_path = 'results/box_muller_1000000.out'
bins, counts = read_data(file_path)

# Paramètres pour la gaussienne
mean = 0  # Remplace par ta valeur
std_dev = 1.0  # Remplace par ta valeur

plot_histogram_with_gaussian(bins, counts, mean, std_dev)
