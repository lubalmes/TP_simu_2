import sys
import matplotlib.pyplot as plt

def read_values_from_file(file_path):
    with open(file_path, 'r') as file:
        values = [float(line.strip()) for line in file]
    return values

def plot_values(values, A, B, K):
    max_value = max(values)
    min_value = min(values)
    avg_value = sum(values) / len(values)
    expected_avg_value = (A + B) / 2
    
    print(f"Max value: {max_value}")
    print(f"Min value: {min_value}")
    print(f"Average value: {avg_value}")
    print(f"Expected average value: {expected_avg_value}")

    plt.hist(values, bins=range(A, B, K))
    
    # plt avg value of values
    plt.axvline(avg_value, color='r', linestyle='dashed', linewidth=2)
    

    plt.xlabel('Values range')
    plt.ylabel('Count')
    plt.title('Histogram with a step of ' + str(K))
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python plot_uniform.py <file_path> <A> <B> <K>")
        sys.exit(1)

    file_path = sys.argv[1]
    A = int(sys.argv[2])
    B = int(sys.argv[3])
    K = int(sys.argv[4])

    values = read_values_from_file(file_path)
    plot_values(values, A, B, K)