import matplotlib.pyplot as plt

def read_data(file_path):
    bins = []
    counts = []
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.split(':')
            bin_range = parts[0].strip()
            count = int(parts[1].strip())
            bins.append(bin_range)
            counts.append(count)
    return bins, counts

def plot_data(bins, counts):
    plt.figure(figsize=(10, 6))
    plt.bar(bins, counts, color='blue')
    plt.xlabel('Bins')
    plt.ylabel('Counts')
    plt.title('Counts per Bin')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    file_path = './results/neg_exp_1000000.out'  # Replace with the actual file path
    bins, counts = read_data(file_path)
    plot_data(bins, counts)