import os
import numpy as np

import matplotlib.pyplot as plt

# Chemin des fichiers
files = ["results/3_distrib_1000.out", "results/3_distrib_10000.out", "results/3_distrib_100000.out", "results/3_distrib_1000000.out"]

# Fréquences attendues
expected_frequencies = {'A': 0.35, 'B': 0.45, 'C': 0.20}

# Initialisation des compteurs
counts = {'A': [], 'B': [], 'C': []}
total_counts = []

# Calcul fréquences des lettres A, B et C, Count(A)/Total, Count(B)/Total, Count(C)/Total
for file in files:
    with open(file, 'r') as f:
        content = f.read()
        count_A = content.count('A')
        count_B = content.count('B')
        count_C = content.count('C')
        total = count_A + count_B + count_C

        counts['A'].append(count_A / total)
        counts['B'].append(count_B / total)
        counts['C'].append(count_C / total)
        total_counts.append(total)

# Création du graphique
labels = ['1000', '10000', '100000', '1000000']
x = np.arange(len(labels))  # l'emplacement des labels
width = 0.2  # la largeur des barres

fig, ax = plt.subplots()
rects1 = ax.bar(x - width, counts['A'], width, label='A')
rects2 = ax.bar(x, counts['B'], width, label='B')
rects3 = ax.bar(x + width, counts['C'], width, label='C')

# Ajout des lignes de fréquence attendue
ax.axhline(y=expected_frequencies['A'], color='blue', linestyle='--', label='Expected A')
ax.axhline(y=expected_frequencies['B'], color='orange', linestyle='--', label='Expected B')
ax.axhline(y=expected_frequencies['C'], color='g', linestyle='--', label='Expected C')

# Ajout des labels, titre et légende
ax.set_xlabel('Nombre d\'individus')
ax.set_ylabel('Fréquence')
ax.set_title('Fréquence des invididus dans chaque classe')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

fig.tight_layout()

plt.show()